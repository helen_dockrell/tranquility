# Data Art By Monthly Theme #

Hi! This is the repo for [my DK30 project](https://day9.tv/dk30/project/5ea019aed3872435a351b0e7)

I am a huge fan of [the Data Sketches site](http://www.datasketch.es/) and want to follow their format to teach myself data visualisation languages. 

## The format is: ##
* Pick a theme word 
* Week 1: Find a relevant data set 
* Week 2: Sketch some designs 
* Week 3: Prototype 
* Week 4: Develop final product

Include sketches/drafts at each update. Theme word: tranquility.

## Running this code yourself: ##

### Week 1: ###
* You will need the free software [R](https://www.r-project.org/) and [RStudio](https://rstudio.com/)
* Download the files in this repository, then open the Visibility_to_PDF.R file in RStudio
* You'll need to set your working directory to let R know where to find your files - change the folder location on line 14.
* Run the code by clicking on the "Run" button on the top right hand side of the user interface.
* The file "monthlyVis.csv" will appear in your 'tranquility' folder location, which contains the clean data.


