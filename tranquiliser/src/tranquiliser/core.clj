(ns tranquiliser.core
  (:require [quil.core :as q])
  (:require [tranquiliser.dynamic :as dynamic]))

(q/defsketch example
  :title "Foreground view"
  :setup dynamic/setup
  :draw dynamic/draw
  :size [1300 975])
