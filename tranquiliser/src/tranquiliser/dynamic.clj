(ns tranquiliser.dynamic
  (:require [quil.core :as q])
  (:require [clojure.java.io :as io])
  (:require [clojure-csv.core :as csv])
  (:require [clojure.string :as str]))


(def foreground (ref nil))

; (defn weatherForecast [today]
;   (with-open [file (io/reader "monthlyVis.csv")]
;     (nth (nth (csv/parse-csv (slurp file)) today) 2)))

(defn fog [today]
  (with-open [file (io/reader "monthlyVis.csv")]
    (nth (nth (csv/parse-csv (slurp file)) today) 2)))

(defn minTemp [today]
  (with-open [file (io/reader "monthlyVis.csv")]
    (nth (nth (csv/parse-csv (slurp file)) today) 3)))

(defn maxTemp [today]
  (with-open [file (io/reader "monthlyVis.csv")]
    (nth (nth (csv/parse-csv (slurp file)) today) 4)))

(defn rainfall [today]
  (with-open [file (io/reader "monthlyVis.csv")]
    (nth (nth (csv/parse-csv (slurp file)) today) 5)))

(defn temp [today]
  (with-open [file (io/reader "monthlyVis.csv")]
    (nth (nth (csv/parse-csv (slurp file)) today) 6)))

(defn humidity [today]
  (with-open [file (io/reader "monthlyVis.csv")]
    (nth (nth (csv/parse-csv (slurp file)) today) 7)))

(defn windDir [today]
  (with-open [file (io/reader "monthlyVis.csv")]
    (nth (nth (csv/parse-csv (slurp file)) today) 8)))

(defn windSpeed [today]
  (with-open [file (io/reader "monthlyVis.csv")]
    (nth (nth (csv/parse-csv (slurp file)) today) 9)))

(defn parse-int [s]
   (Integer. (re-find  #"\d+" s )))

(defn setup []
  (q/smooth)
  (q/frame-rate 6)
  (q/background 200)
  (dosync (ref-set foreground (q/load-image "Foreground MidRes.png"))))


(defn draw []
  (q/stroke (q/random 255))
  (q/stroke-weight (q/random 10))

  (let [diam (q/random 100)
        x    (q/random (q/width))
        y    (q/random (q/height))]
    (q/fill (q/random 255))
    (q/ellipse x y diam diam))

  (q/image @foreground 0 0)

  (let [transp (* 255 0.00005 (parse-int (fog (q/frame-count))))]
    (q/stroke 0)
    (q/stroke-weight 0)
    (q/fill (q/color 159 211 255 transp))
    ;(q/fill (q/color 0 255 transp))
    (q/rect 0 0 1300 975)
  )

  (println (* 255 0.0001 (parse-int (fog (q/frame-count))))))
  ;(println (weatherForecast (q/frame-count))))
